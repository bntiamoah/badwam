import * as admin from "firebase-admin";
import * as functions from "firebase-functions";

admin.initializeApp();

export const loadIncidents = functions.https.onCall(async (data, context) => {
    
  return admin.firestore().collection('incidents').where('creator', '==', context.auth!.uid).get().then((snap) => {
    let result: FirebaseFirestore.DocumentData[] = [];
    snap.forEach((doc) => {
      result.push(doc.data());
    });
    return Promise.resolve(result);
  });
});
