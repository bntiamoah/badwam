import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatCardModule } from '@angular/material/card';

import { AngularFireAuthModule, USE_EMULATOR as USE_AUTH_EMULATOR } from '@angular/fire/compat/auth';
import { AngularFirestoreModule, USE_EMULATOR as USE_FIRESTORE_EMULATOR } from '@angular/fire/compat/firestore';
import { AngularFireFunctionsModule, USE_EMULATOR as USE_FUNCTIONS_EMULATOR } from '@angular/fire/compat/functions';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HelpComponent } from './components/help/help.component';
import { CreateIncidentComponent } from './components/create-incident/create-incident.component';
import { IncidentDetailComponent } from './components/incident-detail/incident-detail.component';
import { MapComponent } from './components/map/map.component';
import { IncidentCommentsComponent } from './components/incident-comments/incident-comments.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { AppEffects } from './state/app.effects';
import { reducer } from './state/app.reducer';
import { AppService } from './state/app.service';
import { DialogContentComponent } from './components/dialog-content/dialog-content.component';
import { AngularFireModule } from '@angular/fire/compat';
import {MatInputModule} from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { SharedModule } from './shared/shared.module';
import { MapModule } from './map/map.module';
import { authReducer } from './auth/state/auth.reducer';

@NgModule({
  declarations: [
    AppComponent,
    HelpComponent,   
    CreateIncidentComponent,
    IncidentDetailComponent,
    MapComponent,
    IncidentCommentsComponent,
    HomeComponent,
    PageNotFoundComponent,
    DialogContentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,

    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatSidenavModule,
    MatListModule,
    MatToolbarModule,
    MatDialogModule,
    MatCardModule,
    MatInputModule,
    MatCheckboxModule,

    SharedModule,
    MapModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireFunctionsModule,

    StoreModule.forRoot(
      { app: reducer }
    ),
    
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),   
    EffectsModule.forRoot([AppEffects]),
    
    !environment.production ? StoreDevtoolsModule.instrument() : []
    
  ],
  providers: [AppService,
    {provide: USE_AUTH_EMULATOR, useValue: environment.useEmulators ? ['http://localhost:9099']: undefined},
    {provide: USE_FIRESTORE_EMULATOR, useValue: environment.useEmulators ? ['http://localhost:8080']: undefined},
    {provide: USE_FUNCTIONS_EMULATOR, useValue: environment.useEmulators ? ['http://localhost:5001']: undefined},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
