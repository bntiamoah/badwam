import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ROUTES_HOME } from 'src/app/app.constants';
import { navigate } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.reducer';
import { SubSink } from 'subsink';
import { login } from '../state/auth.actions';
import { selectIsLoggedIn } from '../state/auth.selectors';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private sub = new SubSink();

  constructor(
    private formBuilder: FormBuilder, private store: Store<AppState>,) { }
 

  loginForm!: FormGroup;

  login() {    
    this.store.dispatch(login({email:  this.loginForm.controls.userName.value, password:  this.loginForm.controls.password.value}))
  }

  ngOnInit(): void {

    this.sub.sink = this.store.select(selectIsLoggedIn).subscribe(isloggin => {
      if (isloggin == true) {
         this.store.dispatch(navigate({route: ROUTES_HOME}))
       }
    })
    
    this.loginForm = this.formBuilder.group({
      userName: ['', Validators.required],
      password: ['', Validators.required],
      rememberMe: [false]
    })
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

}
