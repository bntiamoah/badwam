import { Action, createReducer, on } from '@ngrx/store';
import { loginSuccess, logoutSuccess, registerSuccess } from './auth.actions';

export const authFeatureKey = 'auth';

export interface AuthState {
  isLoggedIn: boolean; 
  isRememberMe: boolean;  
  userName: string;
  registrationSuccess: boolean,
  displayName: string;
}

export const initialState: AuthState = {
  isLoggedIn: false, 
  isRememberMe: false,   
  userName: '',
  registrationSuccess: false,
  displayName: ''
};

export const reducer = createReducer(
  initialState,

    on(registerSuccess, (state, action) => {
    return {
      ...state,
      userName: action.username,
      registrationSuccess: action.success
     }
  }),


  on(loginSuccess, (state, action) => {  
    return {
      ...state,
      isLoggedIn: action.success,
      displayName: action.displayName
     }
  }),
 
  on(logoutSuccess, (state, action) => {
    return {
      ...state,
      isLoggedIn: false,
      displayName: ''
     }
  })
);

export function authReducer(state: AuthState | undefined, action: Action) {
  return reducer(state, action);
}