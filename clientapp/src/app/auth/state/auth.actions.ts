import { createAction, props } from '@ngrx/store';

export const login = createAction(
  '[Auth] Login',
   props<{email: string, password: string}>()
);

export const loginSuccess = createAction(
  '[Auth] Login Success',
  props<{success: boolean, displayName: string}>()
);
export const loginFailure = createAction(
  '[Auth] Login Failure',
  props<{error: string}>()
);

export const register = createAction(
  '[Auth] Register new user',
  props<{reg: any}>()
);

export const registerSuccess = createAction(
  '[Auth] Register success',
  props<{success: boolean, username: string}>()
);

export const logout = createAction(
  '[Auth] Logout'
);

export const logoutSuccess = createAction(
  '[Auth] Logout success'
);






