import { createFeatureSelector, createSelector } from "@ngrx/store";
import { AppState } from "src/app/state/app.reducer";
import { authFeatureKey, AuthState } from "./auth.reducer";

export const selectFeature = createFeatureSelector<AppState, AuthState>(authFeatureKey);

export const selectIsLoggedIn = createSelector(selectFeature,
    (state: AuthState) => {
        return state ? state.isLoggedIn : false;
    });

export const selectUserName = createSelector(selectFeature, (state: AuthState) => state.userName);
export const selectRegisterSuccess = createSelector(selectFeature, (state: AuthState) => state.registrationSuccess);

