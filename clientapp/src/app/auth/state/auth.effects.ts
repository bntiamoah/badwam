import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { setError } from 'src/app/state/app.actions';
import { login, loginSuccess, logout, logoutSuccess, register, registerSuccess } from './auth.actions';
import { AuthService } from './auth.service';



@Injectable()
export class AuthEffects {

  register$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(register),
      switchMap((action) => this.service.createUser(action.reg).pipe(
        map((email: string) => {
          return registerSuccess({ success: true, username: email })
        }),
        catchError(err => of(setError({ error: err })))
      ))
    )
  })

  login$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(login),
      switchMap((action) => this.service.login(action.email, action.password).pipe(
        map((user: any) => {
          return loginSuccess({displayName: user.displayName, success: !!user });
        }),
        catchError(err => of(setError({ error: 'Login Failed!' })))
      ))
    )
  })

  logout$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(logout),
      switchMap((action) => this.service.logout().pipe(
        map((success: boolean) => {          
           return logoutSuccess();  
        })
      ))
    )
  })

  


  constructor(
    private actions$: Actions,
    private service: AuthService,
   ) { }

}
