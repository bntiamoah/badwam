import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { Observable } from 'rxjs';
import { AppService } from 'src/app/state/app.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private auth: AngularFireAuth, private appService: AppService) { }

  createUser(registration: any): Observable<string>{    
    return new Observable(obs => {
      this.auth.createUserWithEmailAndPassword(registration.email, registration.password)
        .then((resp) => {
          resp.user?.updateProfile({ displayName: registration.fullName.split(' ')[0] })
            .then(resp => {
               obs.next(registration.email);
               obs.complete();
            })
            .catch(err => {
              const  errorMessage = err.toString().replace('FirebaseError:', '').replace('Firebase:', '').split('.')[0].trim()
            
              obs.error(errorMessage);
            })         
        }).catch(err => {
          const  errorMessage = err.toString().replace('FirebaseError:', '').replace('Firebase:', '').split('.')[0].trim()
        
          obs.error(errorMessage);
          obs.complete();
        })
    });      
  }

  login(email: string, password: string): Observable<any> {    
    return new Observable(obs => {
      try {        
          this.auth.signInWithEmailAndPassword(email, password)
            .then((resp: any) => {
              this.appService.isLoggedIn = true;
              this.appService.user = resp.user;
            obs.next(resp.user);
            obs.complete();
          }).catch(err => {           
            obs.error(err)
            obs.complete();
        })
        } catch (error) {
           obs.error(error)
            obs.complete();
        }
    })
  }

  logout(): Observable<boolean> {
    return new Observable(obs => {
      try {
        this.appService.isLoggedIn = false;
         this.auth.signOut()
          .then((resp: any) => {
            obs.next(true);
            obs.complete();
          }).catch(err => {
            console.log(err);
            obs.next(false);
            obs.complete();
        })
       } catch (error) {
         console.log(error);
          obs.next(false);
         obs.complete();
       }
    })
  }
}
