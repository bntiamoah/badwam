import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ROUTES_LOGIN } from 'src/app/app.constants';
import { navigate } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.reducer';
import { logout } from '../state/auth.actions';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
     setTimeout(() => {
       this.store.dispatch(logout())
       setTimeout(() => {
         this.store.dispatch(navigate({route: ROUTES_LOGIN}))
       }, 1000);
     }, 1000);
  }

}
