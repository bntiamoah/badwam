import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ROUTES_LOGIN } from 'src/app/app.constants';
import { SharedService } from 'src/app/shared/services/shared.service';
import { navigate, setLoading, showMessage } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.reducer';
import { selectErrors } from 'src/app/state/app.selectors';
import { SubSink } from 'subsink';
import { register } from '../state/auth.actions';
import { selectRegisterSuccess } from '../state/auth.selectors';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  private subsink = new SubSink();

  constructor(
    private formBuilder: FormBuilder,
    private store: Store<AppState>,
    private sharedService: SharedService) { }

  registerForm!: FormGroup;

  register() {
    const match = this.registerForm.controls.password.value ===
      this.registerForm.controls.confirmPassword.value;
    
    if (!match) {
      this.sharedService.showMessage('Passwords do not match');
      return;
    }

    this.store.dispatch(setLoading({ loading: true }));

    const user = {
      fullName: this.registerForm.controls.fullName.value,
      email: this.registerForm.controls.email.value,
      password: this.registerForm.controls.password.value
    }
    this.store.dispatch(register({reg: user}))
   }


  ngOnInit(): void {

    this.subsink.sink = this.store.select(selectRegisterSuccess).subscribe(success => {
      if (success == true) {
        this.store.dispatch(setLoading({ loading: false }));
        this.store.dispatch(navigate({ route: ROUTES_LOGIN }))
      }
    });

    this.subsink.sink = this.store.select(selectErrors).subscribe(errors => {
      if (errors) {
        this.store.dispatch(setLoading({ loading: false }));
        this.store.dispatch(showMessage({ msg: errors, title: "Oops!" }));
       }
    })
    
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });
    
  }

}
