import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { RouterModule, Routes } from '@angular/router';
import { StoreModule } from '@ngrx/store';
import { authFeatureKey, authReducer } from './state/auth.reducer';

import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './state/auth.effects';
import { SharedModule } from '../shared/shared.module';
import { LogoutComponent } from './logout/logout.component';
import { ProfileComponent } from './profile/profile.component';
import { ROUTES_LOGIN } from '../app.constants';
import { AuthGuard } from '../shared/guards/auth.guard';


const routes: Routes = [
    {
        path: 'login',
        component:LoginComponent
    },
    {
        path: 'register',
        component:RegisterComponent
  },
    {
        path: 'logout',
      component: LogoutComponent,
      canActivate: [AuthGuard]
  },
    {
        path: 'profile',
        component: ProfileComponent
  },
  {
    path: "",
    redirectTo: "login",
    pathMatch: "full"
  }
];

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    LogoutComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,

    SharedModule,

    RouterModule.forChild(routes),
    StoreModule.forFeature(authFeatureKey, authReducer),
    EffectsModule.forFeature([AuthEffects]),

    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule
  ],
  exports: [
    LoginComponent,
    RegisterComponent
  ],
  providers: []
})
export class AuthModule { }
