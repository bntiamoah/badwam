import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { setLoading } from '../state/app.actions';
import { AppState } from '../state/app.reducer';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(setLoading({ loading: false }));
  }

}
