import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HelpComponent } from './components/help/help.component';
import { HomeComponent } from './home/home.component';
import { IncidentDetailComponent } from './components/incident-detail/incident-detail.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

import { AngularFireAuthGuard, redirectUnauthorizedTo } from '@angular/fire/compat/auth-guard';
import { ROUTES_LOGIN } from './app.constants';
import { AuthGuard } from './shared/guards/auth.guard';

const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo([ROUTES_LOGIN]);

const routes: Routes = [
  {
    path: 'auth',
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: 'incidents',
    loadChildren: () => import('./incident/incident.module').then(m => m.IncidentModule)
  },
  {
    path: 'home',
    component: HomeComponent,
   canActivate: [AuthGuard]
  },  
  {
    path: 'help',
    component: HelpComponent
  },
  {
    path: ':id',
    component: IncidentDetailComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**', component: PageNotFoundComponent 
  }
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
