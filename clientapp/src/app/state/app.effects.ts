import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import {catchError, map, switchMap, tap} from 'rxjs/operators'
import { LoadingService } from '../shared/loading/loading.service';
import { Incident } from '../shared/model';
import { loadIncidents, loadIncidentsSuccess, navigate, setError, setLoading, setPlatform, showMessage } from './app.actions';
import { AppService } from './app.service';



@Injectable()
export class AppEffects {

  
  setErrors$ = createEffect(() =>
        this.actions$.pipe(
          ofType(setError),
          tap((action) => this.appService.showMessage(action.error, 'Oops!')),
          tap(() => this.loadingService.turnLoadingOff())
    ),{ dispatch: false });  

  navigate$ = createEffect(() =>
        this.actions$.pipe(
          ofType(navigate),
          tap((action) => this.appService.navigate(action.route))
    ),{ dispatch: false });  

  showMessage$ = createEffect(() =>
        this.actions$.pipe(
          ofType(showMessage),
          tap((action) => this.appService.showMessage(action.msg, action.title!))
    ),{ dispatch: false });  
    
  
  setLoading$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(setLoading),
      tap((action) => {
        if (action.loading) {
          this.loadingService.turnLoadingOn();
        } else {
          this.loadingService.turnLoadingOff();
         }
      })     
    )
  }, {dispatch: false})
  
 loadIncidents$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(loadIncidents),
      switchMap((action) => this.appService.loadIncidents().pipe(
        map((incidents: Incident[]) => {
          return loadIncidentsSuccess({data: incidents})
        }),
        catchError(err => of(setError({ error: err })))
      ))
    )
  })

  constructor(private actions$: Actions,
    private appService: AppService,
   private loadingService: LoadingService) { }

}
