import { AppState } from "./app.reducer";

// export const selectIncidents = (state: AppState) => state.incidents;
export const selectErrors = (state: AppState) => state.error;
export const selectLoading = (state: AppState) => state.loading;


