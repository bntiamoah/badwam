import { Injectable } from '@angular/core';
import { AngularFireFunctions } from '@angular/fire/compat/functions';
import { MatDialog } from '@angular/material/dialog';
import { Router, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { DialogContentComponent } from '../components/dialog-content/dialog-content.component';
import { Incident } from '../shared/model';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  
  isLoggedIn!: boolean;
  user: any;

  showMessage2(isMobile: boolean): void {
    console.log(isMobile);
  }
  

  constructor(public dialog: MatDialog, private router: Router,private afFunc: AngularFireFunctions ) { }

  navigate(route: string): void {
    this.router.navigateByUrl(route);
  }

  showMessage(msg: string, title: string) {
    const dialogRef = this.dialog.open(DialogContentComponent, {
      maxWidth: '250px',
      data: {
        title,
        msg
      }
    });
  }

  invokeCallable<T>(callableName: string, data?:any): Promise<T> {
        return new Promise((resolve, reject) => {
            this.afFunc.httpsCallable(callableName)(data)
            .subscribe(result =>{resolve(result as T)}, error =>{ reject(error)})
        });
  }
  
  loadIncidents(): Observable<Incident[]>{   
     return new Observable(obs => {
            try {  
                              
                this.invokeCallable<boolean>('loadIncidents')
                    .then((res:any) => {
                        obs.next(res);                        
                        obs.complete();
                    }).catch(err => {
                        obs.error(err);                       
                        obs.complete();
                    });
            } catch (error) {
                obs.error(error);
               
                obs.complete();
            }
        })
  }

}
