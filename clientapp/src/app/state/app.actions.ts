import { createAction, props } from '@ngrx/store';
import { Incident } from '../shared/model';

export const showMessage = createAction(
  '[App] Show message',
  props<{msg: string, title?: string}>()
)

export const setPlatform = createAction(
  '[App] Set mobile',  
  props<{isMobile: boolean}>()
)

export const setLoading = createAction(
  '[Auth] Set Auth loading status',
  props<{loading: boolean}>()
);

export const navigate = createAction(
  '[Auth] Navigate to route',
  props<{route: string}>()
);

export const setError = createAction(
  '[Auth] Set Errors',
  props<{error: string}>()
);

export const loadIncidents = createAction(
  '[Incident] Load Incidents'
);

export const loadIncidentsSuccess = createAction(
  '[Incident] Load Incidents Success',
  props<{ data: Incident[] }>()
);

export const loadIncidentsFailure = createAction(
  '[Incident] Load Incidents Failure',
  props<{ error: any }>()
);