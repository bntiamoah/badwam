import {
  Action,
  createReducer,
  MetaReducer,
  on} from '@ngrx/store';
import { environment } from '../../environments/environment';
import { AuthState } from '../auth/state/auth.reducer';
import { IncidentState } from '../incident/state/incident.reducer';
import { Incident } from '../shared/model';
import { loadIncidentsSuccess, setError, setLoading, setPlatform } from './app.actions';


export interface AppState {  
  error: string;
  loading: boolean;
  auth: AuthState;
  incident: IncidentState,
  incidents: Incident[];
}

export const initialState: AppState = { 
  error: '',
  loading: false,
  auth: {} as AuthState,
  incident: {} as IncidentState,
  incidents: []
};

const appReducer = createReducer(
  initialState,
  
on(loadIncidentsSuccess, (state, action) => {
    return {
      ...state,
     incidents: action.data
     }
  }),


  on(setError, (state, action) => {
    return {
      ...state,
      error: action.error
     }
  }),

  on(setLoading, (state, action) => {
    return {
      ...state,
      loading: action.loading
     }
  })
);

export function reducer(state: AppState | undefined, action: Action) {
  return appReducer(state, action);
}

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [] : [];
