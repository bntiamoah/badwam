import { AfterViewInit, Component, EventEmitter, Input, Output } from '@angular/core';
import * as L from 'leaflet';
import { MarkerService } from './marker.service';
import { UUID } from 'angular2-uuid';

const iconRetinaUrl = 'assets/marker-icon-2x.png';
const iconUrl = 'assets/marker-icon.png';
const shadowUrl = 'assets/marker-shadow.png';
const iconDefault = L.icon({
  iconRetinaUrl,
  iconUrl,
  shadowUrl,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
  tooltipAnchor: [16, -28],
  shadowSize: [41, 41]
});
L.Marker.prototype.options.icon = iconDefault;

@Component({
  selector: 'app-leaflet-map',
  templateUrl: './leaflet-map.component.html',
  styleUrls: ['./leaflet-map.component.scss']
})
export class LeafletMapComponent implements AfterViewInit {
  private map: any;
  private markersLayer: any;
  private canAddMarker!: boolean;

  constructor(private markerService: MarkerService) { }

  
  private _title: string = "test";
  
  @Input()
  public get title() : string {
    return this._title;
  }
  public set title(v : string) {
    this._title = v;
    this.titleChange.emit(v);
  }

  
  private _canAddIncidentMarker!: boolean;
  
  @Input()
  public get canAddIncidentMarker() : boolean {
    return this._canAddIncidentMarker;
  }
  public set canAddIncidentMarker(v : boolean) {
    this._canAddIncidentMarker = v;
  }

  @Input()
  showEditor!: boolean;

  @Input()
  zoom: number = 6;

  @Output()
  onMapClick = new EventEmitter<any>();

   @Output()
  titleChange = new EventEmitter<string>();

  private initMap(): void {
    // const el = document.createElement('div')
    // const id = UUID.UUID().replace('-', '').replace('-', '').replace('-', '').replace('-', '');
    // el.setAttribute("id", id);

    // document.getElementById('map')?.appendChild(el);

    setTimeout(() => {

      this.map = L.map(document.getElementById('map'), {
      center: [ 8.14462, -1.08438 ],
      zoom: this.zoom
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 15,
      minZoom: 0,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    const self = this;
    this.map.on('click', function (e) {
      if (self.canAddMarker) {
        const pt = e.latlng
        self.onMapClick.emit(pt);

        self.markersLayer.clearLayers();

        self.markerService.makeMarker(self.markersLayer, self.title, pt.lat, pt.lng, null);
        self.canAddMarker = false;
      }
    })

    this.markersLayer = L.featureGroup().addTo(this.map);
    this.markersLayer.on('click', function (e) {
      var clickedMarker = e.layer;
      console.log(clickedMarker);
    })

    tiles.addTo(this.map);
      
    }, 1000);
  }

  edit() {
    this.canAddMarker = true;
  }

  delete() {
    this.markersLayer.clearLayers();
    this.canAddMarker = false;
  }
  ngAfterViewInit(): void {
    this.initMap();
  }
}
