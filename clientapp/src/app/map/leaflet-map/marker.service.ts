import { Injectable } from '@angular/core';
import * as L from 'leaflet';

@Injectable({
  providedIn: 'root'
})
export class MarkerService {

  constructor() { }

  makeMarker(markersLayer: any, content: string, lat: number, lng: number, icon: null) {
       L.marker([lat, lng]).addTo(markersLayer).bindPopup(content);  
  }
}
