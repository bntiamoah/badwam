import { ChangeDetectorRef, Component, OnDestroy } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { ROUTES_CREATE, ROUTES_LOGIN } from './app.constants';
import { Store } from '@ngrx/store';
import { AppState } from './state/app.reducer';
import {SubSink} from 'subsink';
import { OnInit } from '@angular/core';
import { selectIsLoggedIn } from './auth/state/auth.selectors';
import { loadIncidents, navigate } from './state/app.actions';
import { AppService } from './state/app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  
  //incidents$: Observable<Incident[] | null>
  isLoggedIn!: boolean;
  
  title = 'public forum';
  mobileQuery: MediaQueryList;
  private _mobileQueryListener: () => void;
  private subsink = new SubSink();
  
  constructor(
    changeDetectorRef: ChangeDetectorRef,
    media: MediaMatcher,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private appService: AppService,
    private store: Store<AppState>) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');

    this._mobileQueryListener = () => {
    
      changeDetectorRef.detectChanges();
    };

    this.mobileQuery.addEventListener( 'change', this._mobileQueryListener);
    //this.incidents$ = this.store.select(selectIncidents);

    this.subsink.sink = this.store.select(selectIsLoggedIn).subscribe(isLoggedin => {
        console.log('login?', isLoggedin)
      this.isLoggedIn = isLoggedin
      if (this.isLoggedIn) {
         this.store.dispatch(loadIncidents())
      }
    })
    
  }

  createIncident() {
    this.store.dispatch(navigate({route: ROUTES_CREATE}))
  }

  ngOnInit(): void {
    // this.subsink.sink = this.store.select(selectErrors).subscribe(error => {
    //   if (error) {
    //     this.store.dispatch(showMessage({ title: 'Request failed', msg: error }));
    //   }
    // });

     //this.store.dispatch(showMessage({ title: 'Request failed', msg: 'dfadf' }));
    
    //this.store.dispatch(setLoading({loading: true}))
  }

  logout() {
    //this.store.dispatch(logout())
    this.router.navigateByUrl(ROUTES_LOGIN)
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener('change', this._mobileQueryListener);
    this.subsink.unsubscribe();
  }
}
