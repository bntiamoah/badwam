import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'public forum'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('public forum');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(compiled.querySelector('.content span')?.textContent).toContain('public forum app is running!');
  });

  it('should allow users to create account', () => {    
    expect(true).toBe(true);
  });
  it('should allow users to create edit account', () => {    
    expect(true).toBe(true);
  });
  it('should allow users to create delete account', () => {    
    expect(true).toBe(true);
  });
  it('should allow users to login into the application', () => {    
    expect(true).toBe(true);
  });
  it('should allow users to create issue', () => {    
    expect(true).toBe(true);
  });
  it('should allow users to edit issue', () => {    
    expect(true).toBe(true);
  });
  it('should allow users to delete only authored issue', () => {    
    expect(true).toBe(true);
  });
  it('should not allow users to delete someone else"s issue', () => {    
    expect(true).toBe(true);
  });
  it('should allow users to view all issues', () => {    
    expect(true).toBe(true);
  });
});
