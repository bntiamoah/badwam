import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-dialog-content',
  templateUrl: './dialog-content.component.html',
  styleUrls: ['./dialog-content.component.scss']
})
export class DialogContentComponent  {

  constructor(
    private dialogRef: MatDialogRef<DialogContentComponent>,
    @Inject(MAT_DIALOG_DATA) data: any) { 
    this.title = data.title;
    this.msg = data.msg;
    this.responseRequired = data.responseRequired;
    }

  title!: string;
  msg!: string;
  responseRequired!: boolean;

  respond(response?: string) {
    if (response === 'Yes') {
      this.dialogRef.close('Yes');
      return;
    }

    if (response === 'Yes') {
      this.dialogRef.close('Yes');
      return;
    }

    this.dialogRef.close();
  }

}
