import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ROUTES_HOME } from 'src/app/app.constants';
import { navigate } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.reducer';

@Component({
  selector: 'app-help',
  templateUrl: './help.component.html',
  styleUrls: ['./help.component.scss']
})
export class HelpComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    setTimeout(() => {
       this.store.dispatch(navigate({route: ROUTES_HOME}));
    }, 5000);
  }

}
