export enum IncidentStatus{
    NEW = 'Submitted',
    PROGRESS = 'In Progress',
    RESOLVED = 'Resolved'
}

export interface Comment{
    displayName: string;
    comment: string;
}

export interface Location{
    latitude: number;
    longitude: number;
    x: number;
    y: number;
}

export interface Incident{
    id: string;
    creator: string;
    createAt: string;
    status: IncidentStatus;
    comments: Comment[];
    title: string;
    description: string;
    votes: number;
    location: Location;
    isClosed: boolean;
    updatedAt: string;
}