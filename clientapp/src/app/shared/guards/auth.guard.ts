import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { ROUTES_LOGIN } from 'src/app/app.constants';
import { AppService } from 'src/app/state/app.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private appService: AppService, private router: Router){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot):  boolean  {
  
    if (this.appService.isLoggedIn) {
      return true;
    }
    this.router.navigateByUrl(ROUTES_LOGIN);
    return false;
  }
  
}
