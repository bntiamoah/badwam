import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  constructor() { }

  showMessage(msg: string) {
    console.log(msg);
  }

  showLoading(loading: boolean) {
    
  }
}
