import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { tap, concatMap, finalize } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LoadingService {
    private subject = new BehaviorSubject<boolean>(false);

    loading$: Observable<boolean> = this.subject.asObservable();

    showLoadingUntilCompletion<T>(obs$: Observable<T>): Observable<T> {
        return of(null)
            .pipe(
                tap(() => this.turnLoadingOn()),
                concatMap(() => obs$),
                finalize(() => {
                    console.log('Finalize entered');
                    this.turnLoadingOff();
                })
            );
    }

    turnLoadingOff(): void {
        this.subject.next(false);
    }
    turnLoadingOn(): void {
        this.subject.next(true);
    }
}
