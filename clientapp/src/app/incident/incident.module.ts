import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Route, RouterModule } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MapModule } from '../map/map.module';
import { HomeComponent } from '../home/home.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { incidentFeatureKey, incidentReducer } from './state/incident.reducer';
import { IncidentEffects } from './state/incident.effects';
import { AuthGuard } from '../shared/guards/auth.guard';

const routes: Route[] = [
  {
    path: '',
    component: HomeComponent,
     canActivate: [AuthGuard]
  },
   {
    path: 'create',
     component: CreateComponent,
     canActivate: [AuthGuard]
  }
]

@NgModule({
  declarations: [
    CreateComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

    ReactiveFormsModule,
    FormsModule,

    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatCheckboxModule,
    MapModule,

    StoreModule.forFeature(incidentFeatureKey, incidentReducer),
    EffectsModule.forFeature([IncidentEffects]),

  ],
  exports: [
    CreateComponent
  ]
})
export class IncidentModule { }
