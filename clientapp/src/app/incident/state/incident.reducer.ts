import { Action, createReducer, on } from '@ngrx/store';
import { Incident } from 'src/app/shared/model';

export const incidentFeatureKey = 'incident';

export interface IncidentState {
  incidents: Incident[];
}

export const initialState: IncidentState = {
  incidents: []
};

export const IncReducer = createReducer(
  initialState,
  
);

export function incidentReducer(state: IncidentState | undefined, action: Action) {
  return IncReducer(state, action);
}