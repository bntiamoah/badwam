import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';
import { Incident } from 'src/app/shared/model';
import { AppService } from 'src/app/state/app.service';


@Injectable({
  providedIn: 'root'
})
export class IncidentService {

  constructor(private appService: AppService, private firestore: AngularFirestore) { }  

  createIncident(incident: Incident): Observable<void> {   
     return new Observable(obs => {
       try {
            
           this.firestore.collection('incidents').add(incident)
          .then(res => {
            console.log(res);
              obs.next()
              obs.complete()
          })
          .catch(e => {
            console.log(e);
            obs.next()
            obs.complete()
          })
            
          } catch (error) {
             console.log(error);
            obs.next()
            obs.complete()
          }
      })
  }
  
  closeIncident(incidentId: string) : Observable<void> {
    return new Observable(obs => {
        const docRef = this.firestore.collection('incidents', ref => ref.where("id", "==", incidentId));
        docRef.get().subscribe(ss => {
          ss.forEach(changes => {
            const data: any = changes.data();
            data.isClosed = true;
            docRef.doc().update(data);
            obs.next()
            obs.complete();
              })
          })
      })
  }
  
  
}
