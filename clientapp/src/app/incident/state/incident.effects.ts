import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore } from '@angular/fire/compat/firestore';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { tap } from 'rxjs/operators';
import { AppState } from 'src/app/state/app.reducer';
import { closeIncident, createIncident } from './incident.actions';
import { IncidentService } from './incident.service';



@Injectable()
export class IncidentEffects {

  

  createIncident$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(createIncident),
      tap((action) => this.service.createIncident(action.data).subscribe())
    )
  }, { dispatch: false });
  
  closeIncident$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(closeIncident),
      tap((action) => this.service.closeIncident(action.incidentId).subscribe())
    )
  }, { dispatch: false });  

  constructor(
    private actions$: Actions,
     private afauth: AngularFireAuth,
    private service: IncidentService,
     private store: Store<AppState>,
     private afs: AngularFirestore) {
    
      // this.afauth.authState.pipe(map(user => {

      //       if (!user) return;

      //   if (user?.uid) {
              
      //     this.afs.doc(`incidents/${user!.uid}`).valueChanges().subscribe((d: any) => {
      //           d.pipe(item => console.log(item))
      //           console.log(d);
      //             //  if (d) {
      //             //    this.store.dispatch(loadIncidentsSuccess({
      //             //       data: d
      //             //   }));
      //             // }
      //           });
      //        }           

      //   })).subscribe();  
    
    }

}
