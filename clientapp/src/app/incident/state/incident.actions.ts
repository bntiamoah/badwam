import { createAction, props } from '@ngrx/store';
import { Incident } from 'src/app/shared/model';

export const createIncident = createAction(
  '[Incident] Create Incident',
  props<{ data: Incident }>()
);

export const closeIncident = createAction(
  '[Incident] Close Incident',
  props<{ incidentId: string }>()
);



