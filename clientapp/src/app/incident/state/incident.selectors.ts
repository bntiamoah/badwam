import { createFeatureSelector, createSelector } from "@ngrx/store";
import { AppState } from "src/app/state/app.reducer";
import { incidentFeatureKey, IncidentState } from "./incident.reducer";

export const selectFeature = createFeatureSelector<AppState, IncidentState>(incidentFeatureKey);

export const selectIncidents = createSelector(selectFeature, (state: IncidentState) => state.incidents);
