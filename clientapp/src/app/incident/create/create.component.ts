import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { ROUTES_HOME } from 'src/app/app.constants';
import { Incident, IncidentStatus, Location } from 'src/app/shared/model';
import { navigate, setLoading } from 'src/app/state/app.actions';
import { AppState } from 'src/app/state/app.reducer';
import { AppService } from 'src/app/state/app.service';
import { createIncident } from '../state/incident.actions';
import { UUID } from 'angular2-uuid';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  latitude: any;
  longitude: any;

  constructor(private formBuilder: FormBuilder,
    private store: Store<AppState>, private appService: AppService) { }
  
  createForm!: FormGroup;
  
  create() {  
    const incident = {
      id :  UUID.UUID(),
      title: this.createForm.controls.title.value,
      description: this.createForm.controls.description.value,
      location: {
        latitude: +this.latitude,
        longitude: +this.longitude,
        x: +this.longitude,
        y: +this.latitude
      } as Location,
      creator: this.appService.user.uid,
      createdAt: new Date().toISOString(),
      status: IncidentStatus.NEW,
      commennts: [],
      votes: 0,
      isClosed: false,
      updatedAt: null
    } as unknown as Incident;

    this.store.dispatch(createIncident({ data: incident }));

    this.store.dispatch(navigate({route: ROUTES_HOME}));
   }
  
  Cancel() {
    this.store.dispatch(navigate({ route: ROUTES_HOME }));
  }
  
  onMapClick(e) {
    this.latitude = e.lat;
    this.longitude = e.lng;
    this.createForm.controls.location.setValue(`Latitude: ${e.lat}    Longitude: ${e.lng}`)
  }

  ngOnInit(): void {

    this.createForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required],
      location: ['', Validators.required]
    });


  }

}

